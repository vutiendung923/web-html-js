var khoahoc = [
    {
        id:11,
        title:"Bài 1: Nhập môn Excel",
        description:`
        <ol>
		<li><p><b>Tạo workbook trong Excel</b></p></li>
		<p>Tạo một workbook:</p>
		<ul>
			<li>Mở Excel.</li>
			<li>chọn Blank workbook hoặc nhấn Ctrl+N.</li>
		</ul>
		<p>Nhập dữ liệu:</p>
		<ul>
			<li>Chọn ô trống, ví dụ A1, rồi gõ text hoặc số.</li>
			<li>Nhấn Enter hoặc tab để chuyển sang ô tiếp theo.</li>
		</ul>
		<li><p><b>Lưu workbook vào OneDriver trong Excel</b></p></li>
		<p>Lưu workbook vào OneDriver:</p>
		<ul>
			<li>Chịn File -> Save As</li>
			<li>Nhập tên file và chọn Save</li>
		</ul>
		<li><p><b>Phân tích định dạng trong Excel</b></p></li>
		<p>Tự động điền một cột bằng Flash Fill:</p>
		<ul>
			<li>Trong ô dưới First Name, gõ Molly và nhấn Enter.</li>
			<li>trong ô tiếp theo, gõ một vài chữ cái trong tên Garret.</li>
			<li>Khi danh sách các giá trị đề xuất hiện ra, nhấn Return. Chọn Flash Fill Options để có nhiều lựa chọn hơn.</li>
		</ul>
		<p>Tính toán nhanh bằng hàm AutoSum:</p>
		<ul>
			<li>Chọn ô bên dưới số bạn muốn thêm.</li>
			<li>Chọn Home -> AutoSum -> The AutoSum.</li>
			<li>Nhấn Enter.</li>
		</ul>
		<p>Tạo sơ đồ:</p>
		<ul>
			<li>Chọn dữ liệu muốn hiển thị trong sơ đồ.</li>
			<li>Chọn nút Quick Analysis ở bên phải phía dưới các ô lựa chọn.</li>
			<li>Chọn Charts, trỏ chuột qua các lựa chọn có sẵn, rồi tích vào sơ đồ mong muốn.</li>
		</ul>
		<li><p><b>Hợp tác trong Excel</b></p></li>
		<p>Chia sẻ workbook với người khác</p>
		<ul>
			<li>Chọn Share trên ribbon hoặc File -> Share. Lưu ý: nếu chưa lưu tập tin của bạn vào OneDriver, microsoft Excel sẽ nhắc bạn upload file để chia sẻ nó.</li>
			<li>Chọn người bạn muốn chia sẻ từ menu thả xuống hoặc nhập tên hay địa chỉ email.</li>
			<li>thêm tin nhắn (tùy chọn) và nhân Send.</li>
		</ul>
		<li><p><b>Cài đặt Excel trên mobile</b></p></li>
		<p>Để tải và sử dụng file ở khắp mọi nơi - cơ quan, tại nhà hay trên đường đi, hãy cài đặt Microsoft Excel trên thiết bị di động. Lựa chọn bản cài cho thiết bị của bạn:</p>
		<ul>
			<li>Microsoft Excel</li>
			<li>Microsoft Excel cho Android</li>
			<li>Microsoft Excel cho iOS</li>
		</ul>
	</ol>
        `
    },
    {
        id:12,
        title:"Bài 2: Những thao tác cơ bản trong Excel",
        description:`
        <ol>
		<li><b>Tạo một workbook mới</b></li>
			<p>Tạo workbook:</p>
			<ul>
				<li>Mở Excel.</li>
				<li>Chọn Blank workbook hoặc nhấn Ctrl+N.</li>
				<li>Bắt đầu gõ.</li>
			</ul>
			<p>Tạo workbook từ template:</p>
			<ul>
				<li>Chọn file -> New.</li>
				<li>Click đúp vào một template.</li>
				<li>Cilck và bắt đầu gõ.</li>
			</ul>
		<br><li><b>Chen hoặc xóa một worksheet</b></li>
			<p>Chèn một worksheet</p>
			<ul>
				<li>Chọn + ở góc dưới màn hình.</li>
				<li>Hoặc chọn Home -> Insert -> Insert Sheet.</li>

			</ul>
			<p>Đổi lại tên wroksheet: </p>
			<ul>
				<li>Click đúp vào tên bảng tính trong tab Sheet để đổi nhanh lại tên của nó.</li>
				<li>Hoặc click đúp vào tab Sheet, click Rename và gõ tên mới.</li>
			</ul>
			<p>Xóa một Worksheet:</p>
			<ul>
				<li>Click chuột phải vào tab Sheet và chọn Delete.</li>
				<li>Hoặc chọn sheet đó, rồi chọn Home -> Delete -> Delete Sheet.</li>
			</ul>
		<br><li><b>Di chuyển/sao chép worksheet hoặc dữ liệu trong worksheet</b></li>
			<p><b>Worksheet</b></p>
			<p>Bạn có thể di chuyển hoặc sao chép một worksheet trong cùng workbook để sắp xếp nó chính xác như ý muốn.</p>
			<p><b>Thận trọng:</b>Khi chuyển một sheet sang workbook khác, hãy kiểm tra công thức hay biểu đồ tham chiếu dữ liệu trên sheet đó bởi hành động này có thể gây lỗi hoặc dẫn tới kết quả ngoài dự đoán trong dữ liệu. Tương tự, nếu chuyển một sheet được tham chiếu 3-D, việc tính toán có thể bao gồm hoặc bỏ qua dữ liệu trên sheet.</p>
			<p><b>Windows</b></p>
			<p>Bạn có thể dùng lệnh Move hoặc Copy Sheet để di chuyển hoặc sao chép toàn bộ worksheet tới vị trí khác trong cùng hoặc khác workbook. Bạn có thể dùng lệnh Cut & Copy để di chuyển/sao chép một phần dữ liệu lên worksheet hoặc workbook khác.</p>
		<br><li><b>Điền dữ liệu tự động vào các ô trong worksheet</b></li>
			<p>Dùng tính năng Auto Fill để điền dữ liệu vào các ô theo mẫu hoặc dựa trên dữ liệu trong ô khác</p>
			<ul>
				<li>Chọn một hoặc nhiều ô bạn muốn để điền dữ liệu. </li>
				<li>Kéo thanh điền dữ liệu </li>
				<li>Nếu cần, click Auto Fill Options và click tùy chọn bạn muốn. Trên đây là những thao tác nhập, điền, sao chép và di chuyển dữ liệu trong bảng tính Excel.</li>
			</ul>
	</ol>
        `  
    },
    {
        id:13,
        title:"Bài 3: In worksheet hoặc workbook",
        description:`
        <ol>
		<li><b>Windows</b></li>
		<p>In một hoặc nhiều worksheet</p>
		<ul>
			<li>Chọn worksheet muốn in.</li>
			<li>Click File > Print hoặc nhấn CTRL+P.</li>
			<li>Click nút Print hoặc điều chỉnh Settings trước khi click nó.</li>
		</ul>
		<p>In một hoặc nhiều workbook</p>
		<ul>
			<li>Click File > Open.</li>
			<li>Giữ phím Ctrl, click tên từng workbook để in, rồi nhấn Print.</li>
		</ul>
		<p>In toàn bộ hoặc một phần worksheet</p>
		<ul>
			<li>Click worksheet, rồi chọn phạm vi dữ liệu muốn in.</li>
			<li>Click File > Print.</li>
			<li>Trong Settings, click mũi tên bên cạnh Print Active Sheets và tích lựa chọn phù hợp.</li>
			<li>Click Print.</li>
		</ul>
		<br><li><b>Web</b></li>
		<p>In worksheet</p>
		<ul>
			<li>1.	Click File > Print > Print.</li>
			<li>2.	Nếu đã chọn một phạm vi ô nhưng quyết định in toàn bộ worksheet, chuyển sang Entire Workbook trước khi nhấn Print.</li>
			<li></li>
			<li></li>
		</ul>
		<p>Thay đổi vùng in lựa chọn</p>
		<ul>
			<li>1.	Trên worksheet, click và kéo để chọn dữ liệu muốn in.</li>
			<li>2.	Click File > Print > Print.</li>
			<li>3.	Để chỉ in vùng lựa chọn, trong Print Options > click Current Selection.</li>
			<li>4.	Nếu cửa sổ xem trước hiển thị dữ liệu bạn muốn in, click Print.</li>
		</ul>
		<p>In worksheet đã ẩn hàng và cột</p>
		<ul>
			<li>Chọn phạm vi tiêu đề bao quanh hàng hoặc cột bị ẩn. </li>
			<li>Click chuột phải vào lựa chọn và tích Unhide Rows (đối với cột, chọn Unhide Columns).</li>
			<li>Click File > Print.</li>
			<li>Click Print để mở cửa sổ xem trước.</li>
		</ul>
	    </ol>`
    },
    {
        id:14,
        title:"Bài 4: Dùng Excel làm máy tính",
        description:`
        <p><b>Chi tiết cách dùng công thức cơ bản trong Excel</b></p>
	    <p>Toàn bộ công thức bắt đầu bằng dấu =. Đối với các công thức đơn giản, bạn chỉ cần gõ dấu bằng, theo sau là các giá trị số muốn tính toán và phép tính muốn sử dụng, cụ thể: dấu + để thêm, dấu - để trừ, dấu * để nhân, dấu / để chia. Sau đó, nhấn ENTER, Excel ngay lập tức tính và hiển thị kết quả của công thức.</p>
	    <p><b>Dùng AutoSum</b></p>
	    <p>Cách dễ nhất để thêm công thức SUM vào worksheet là dùng AutoSum. Trực tiếp chọn ô trống ở trên hoặc dưới phạm vi bạn muốn tính tổng. Trên Home hoặc các tab Formula của ribbon, click AutoSum > Sum. AutoSum sẽ tự động nhận diện phạm vi cần tính tổng và xây dựng công thức cho bạn. Tác vụ này cũng hoạt động theo chiều ngang nếu bạn chọn ô bên trái hoặc phải của phạm vi cần tính tổng.</p>
	    <p><b>Dùng AutoSum cho cột dữ liệu (theo chiều dọc)</b></p>
	    <p>Trong bảng tính trên, tính năng AutoSum tự động phát hiện các ô B2:B5 là phạm vi tính tổng. Toàn bộ việc bạn cần làm là nhấn ENTER để xác thực nó. Nếu cần thêm/loại bỏ nhiều ô hơn, bạn có thể giữ Shift + phím mũi tên cần thiết cho tới khi lựa chọn của bạn phù hợp với thứ bạn muốn. Sau đó, nhấn Enter để hoàn thành nhiệm vụ.</p>
	    <p><b>Dùng AutoSum cho hàng dữ liệu (theo chiều ngang)</b></p>`
    },
    {
        id:15,
        title:"Bài 5: Hàng & cột trong Microsoft Excel",
        description:`
        <ol>
		<li><b>Chèn hoặc xóa hàng & cột</b></li>
			<p>Chèn hoặc xóa cột</p>
			<ul>
				<li>Chọn ô bất kỳ trong cột, rồi tới Home > Insert > Insert Sheet Columns hoặc Delete Sheet Columns./li>
				<li>Cách khác, click chuột phải vào phía trên cùng của cột, rồi chọn Insert hoặc Delete.</li>
			</ul>
			<p>chèn hoặc xóa hàng</p>
			<ul>
				<li>Chọn ô bất kỳ trong hàng, sau đó, tới Home > Insert > Insert Sheet Columns hoặc Delete Sheet Columns.</li>
				<li>Cách khác, click chuột phải vào số hàng, rồi chọn Insert hoặc Delete.</li>
			</ul>
		<br><li><b>Chọn nội dung ô trong Excel</b></li>
			<p>Chọn một hoặc nhiều ô</p>
			<ul>
				<li>Click vào ô muốn chọn hoặc dùng bàn phím điều hướng tới ô đó & chọn nó.</li>
				<li>Để chọn một phạm vi, chọn một ô, rồi nhấn nút chuột trái, kéo qua các ô khác. Hoặc dùng Shift + các phím mũi tên để chọn vùng.</li>
				<li>Để chọn các ô & phạm vi ô không liền kề, giữ Ctrl, rồi chọn những ô đó.</li>
			</ul>
			<p>Chọn một hoặc nhiều hàng, cột</p>
			<ul>
				<li>Click vào chữ cái ở phía trên cùng của cột để chọn toàn bộ nội dung trong đó. Hoặc click vào ô bất kỳ trong cột, rồi nhấn Ctrl + Phím cách.</li>
				<li>Nhấn vào số hàng để chọn toàn bộ hàng đó hoặc click ô bất kỳ trong hàng, rồi nhấn Shift + Phím cách.</li>
				<li>Để chọn ô hoặc cột không liền kề, giữ Ctrl và chọn số hàng hoặc cột.</li>
			</ul>
			<p>Chọn bảng biểu, danh sách hoặc worksheet</p>
			<ul>
				<li>Để chọn danh sách hoặc bảng biểu, chọn ô trong nằm trong nó và nhấn Ctrl +A.</li>
				<li>Để chọn toàn bộ worksheet, click nút Select All ở góc trên cùng bên trái.</li>
			</ul>
		<br><li><b>Đóng băng hàng hoặc cột</b></li>
			<p>Đóng băng cột đầu tiên</p>
			<ul>
				<li>Chọn View > Freeze Panes > Freeze First Column.</li>
			</ul>
			<p>Đóng băng 2 cột đầu tiên</p>
			<ul>
				<li>Chọn cột thứ ba.</li>
				<li>Chọn View > Freeze Panes > Freeze Panes. </li>
			</ul>
		<br><li><b>Bỏ đóng băng hàng hoặc cột</b></li>
			<br>
			<ul>
				<li>Trên tab View > Window > Unfreeze Panes.</li>
			</ul>
			<br>
		<li><b>Ẩn hoặc hiện hàng/cột</b></li>
			<p>Ẩn cột</p>
			<ul>
				<li>Chọn một hoặc nhiều cột, nhấn Ctrl để chọn thêm các cột không liền kề khác.</li>
				<li>Click chuột phải vào cột đã chọn, rồi nhấn Hide.</li>
			</ul>
			<p>Bỏ ẩn cột</p>
			<ul>
				<li>Chọn các cột liền kề với cột bị ẩn.</li>
				<li>Click chuột phải vào cột lựa chọn, rồi click Unhide. Cách khác, click đúp vào đường thẳng kép nằm giữa hai cột tại vị trí cột ẩn tồn tại.</li>
			</ul>
	    </ol>`
    },
    {
        id:16,
        title:"Bài 6: Cách lọc giá trị duy nhất và xóa giá trị trùng lặp trong Excel",
        description:`
        <p>Lọc giá trị duy nhất hay loại bỏ giá trị trùng lặp là hai nhiệm vụ tương tự nhau bởi mục tiêu đều hướng tới trình bày một danh sách các giá trị riêng biệt. Tuy nhiên, có một sự khác biệt quan trọng. Khi bạn lọc các giá trị duy nhất, giá trị trùng lặp chỉ bị ẩn tạm thời. Xóa giá trị trùng lặp đồng nghĩa chúng bị loại vĩnh viễn khỏi bảng tính.</p>
        <p>Giá trị trùng lặp là một trong số tất cả giá trị nằm tối thiểu ở một hàng đồng nhất với tất cả giá trị của hàng khác. So sánh các giá trị trùng lặp phụ thuộc vào dữ liệu hiện trong ô - không phải giá trị lưu trong ô. Ví dụ, bạn có cùng giá trị ngày ở các ô khác nhau. Một ô có định dạng 3/8/2006, ô khác là Mar 8, 2006. Những giá trị này là duy nhất.</p>
        <p><b>Cách lọc giá trị duy nhất:</b></p>
        <ul>
            <li>Chọn phạm vi ô hoặc đảm bảo ô đang sử dụng nằm trong một bảng.</li>
            <li>Click Data > Advanced (trong nhóm Sort & Filter).</li>
            <li>Ở popup Advanced Filter</li>
        </ul>
        <p><b>Định dạng nhanh</b></p>
        <ul>
            <li>Chọn một hoặc nhiều ô hơn trong một phạm vi, bảng biểu hoặc PivotTable.</li>
            <li>Trên tab Home, trong nhóm Style, click mũi tên nhỏ ở Conditional Formatting > Highlight Cells Rules và chọn Duplicate Values.</li>
            <li>Nhập giá trị bạn muốn dùng, rồi chọn định dạng.</li>
        </ul>`
    },
    {
        id:17,
        title:"Bài 7: Di chuyển & sao chép nội dung ô trong Excel",
        description:`
        <p><b>Di chuyển ô bằng cách kéo & thả</b></p>
        <ul>
            <li>Chọn ô hoặc phạm vi ô muốn sao chép hay di chuyển.</li>
            <li>Trỏ chuột tới đường viền bao quanh lựa chọn.</li>
            <li>Khi con trỏ có hình dạng, kéo ô hoặc phạm vi ô đó sang vị trí khác.</li>
        </ul>
        <p> <b>Di chuyển ô bằng cách Cut & Paste</b></p>
        <ul>
            <li>Chọn ô hoặc phạm vi ô.</li>
            <li>Chọn Home -> Cut hoặc nhấn Ctrl + X.</li>
            <li>Chọn ô tại vị trí bạn muốn chuyển dữ liệu.</li>
            <li>Chọn Home -> Paste hoặc nhấn Ctrl + V.</li>
        </ul>
        <p><b>Di chuyển hoặc sao chép ô</b></p>
        <ul>
            <li>Chọn ô muốn chuyển hoặc sao chép.</li>
            <li>Trên tab Home, trong nhóm Clipboard, thực hiện một trong số tác vụ sau:</li>
            <li>Chọn ô phía trên bên trái của vùng dán dữ liệu.</li>
            <li>Trên tab Home, trong nhóm Clipboard, click Paste hoặc nhấn Ctrl+V.</li>
        </ul>
        <p><b>Di chuyển hoặc sao chép ô bằng chuột</b></p>
        <ul>
            <li>Chọn ô hoặc phạm vi ô muốn sao chép hay di chuyển.</li>
            <li>Sao chép một ô hoặc phạm vi ô, giữ phím Ctrl trong khi bạn trỏ tới đường viền lựa chọn. Khi con trỏ chuyển sang, kéo ô hoặc phạm vi ô đó sang vị trí khác.</li>
        </ul>
        <p><b>Sao chép chỉ các ô hiển thị</b></p>
        <ul>
            <li>Chọn ô muốn sao chép.</li>
            <li>Trên tab Home > Editing > click Find & Select > Go To Special.</li>
            <li>Trong Select, click Visible cells only > OK.</li>
            <li>Trên tab Home > Clipboard > click Copy hoặc nhấn Ctrl+C.</li>
            <li>Chọn ô phía trên bên trái vùng dán.</li>
            <li>Trên tab Home > Clipboard > click Paste hoặc nhấn Ctrl+V.</li>
        </ul>`
    },
    {
        id:18,
        title:"Bài 8: Tìm & thay thế text, số trên worksheet",
        description:`
        <ol>
		<li><b>Tìm dữ liệu</b></li>
		<ul>
			<li>Trong box Find what:, gõ text hoặc số bạn muốn tìm hay click mũi tên trong box này, rồi chọn mục tìm kiếm gần đây từ danh sách.</li>
			<li>Click Find All hoặc Find Next để chạy tìm kiếm.</li>
			<li>Click Options>> để tìm kiếm sâu hơn nếu cần</li>
			<li>Nếu muốn tìm text hoặc số chứa định dạng cụ thể, click Format, sau đó tiến hành lựa chọn trong box hộp thoại Find Format.</li>
		</ul>
		<br><li><b>Thay thế dữ liệu</b></li>
		<ul>
			<li>Tìm box Find what:, gõ text hoặc số muốn tìm hoặc click mũi tên trong box Find what:, rồi chọn mục tìm kiếm gần đây từ danh sách này.</li>
			<li>Click Replace All hoặc Replace.</li>
			<li>Click Options>> mở rộng kết quả tìm kiếm nếu cần.</li>
			<li>Nếu muốn tìm text hoặc số ở định dạng cụ thể, click Format, rồi lựa chọn định dạng bạn muốn trong box Find Format.</li>
		</ul>
	    </ol>`
    },
    {
        id:19,
        title:"Bài 9: Cách gộp và bỏ gộp ô trong Excel",
        description:`
        <ol>
		<li><b>Cách gộp ô</b></li>
		<p>Để gộp ô Excel, chọn ô muốn hợp nhất. Bạn có thể chọn số lượng ô bất kỳ. Ở ví dụ này, chúng tôi chỉ chọn hai ô. Khi chọn xong, hãy đi tới tab Home, rồi click Merge & Center ở phần Alignment.</p>
		<br><li><b>Kết hợp ô bằng CONCATENATE</b></li>
		<p>Thực tế, mất dữ liệu là nhược điểm lớn khi sử dụng tính năng gộp ô trong Excel. Tuy nhiên, bạn có thể tránh điều đó bằng cách dùng công thức CONCATENATE. Đây là một trong những tính năng hữu ích nhất của Excel: =CONCATENATE(text 1, [text 2],...).</p>
		<p>Tính năng này tập hợp nhiều nội dung và tạo một ô mới, kết hợp tất cả lại. Nó cho phép bạn gộp ô mà không mất dữ liệu. Ví dụ: Áp dụng nó ở cột First và Last Name trong bảng tính trên, thay vì hợp nhất ô làm mất tên họ, chúng tôi sẽ tạo cột mới, rồi sử dụng CONCATENATE để lấy cả hai dữ liệu họ tên.</p>
		<br><li><b>Cách bỏ gộp ô</b></li>
		<p>Nếu quyết định gộp ô Excel, bạn sẽ muốn biết cách gỡ bỏ chúng.</p>
		<p>Thật đáng tiếc, bỏ gộp ô không giúp bạn lấy lại dữ liệu. Khi gộp ô trong Excel, bạn sẽ mất một số thông tin mãi mãi. Các ô đã gộp có thể tạo không gian lạ trong bảng tính và bỏ gộp chúng sẽ giải quyết vấn đề này.</p>
		<p>Để bỏ gộp ô, chỉ cần chọn ô đã gộp, click mũi tên đi xuống bên cạnh Merge & Center > Unmerge Cells.</p>
	    </ol>`
    },
    {
        id:20,
        title:"Bài 10: Cách sử dụng Data Validation trong Excel",
        description:`
        <ol>
		<li>Chọn ô bạn muốn tạo quy tắc.</li>
		<br><li>Chọn Data >Data Validation.</li>
		<br><li>Trên tab Settings, trong Allow, tích một tùy chọn:</li>
			<ul>
				<li>Whole Number - Hạn chế ô chỉ chấp nhận các giá trị số nguyên.</li>
				<li>Decimal - Hạn chế ô chỉ chấp nhận số thập phân.</li>
				<li>List - Chọn dữ liệu từ danh sách thả xuống.</li>
				<li>Date - Hạn chế ô chỉ chấp nhận giá trị ngày.</li>
				<li>Time - Hạn chế ô chỉ chấp nhận giá trị thời gian.</li>
				<li>Text Length - Hạn chế độ dài văn bản.</li>
				<li>Custom - Tùy biến công thức.</li>
			</ul>
		<br><li>Trong Data, chọn một điều kiện:</li>
			<ul>
				<li>between - Ở giữa</li>
				<li>not between - Không phải ở giữa</li>
				<li>equal to - Bằng</li>
				<li>not equal to - Không bằng</li>
				<li>greater than - Lớn hơn</li>
				<li>less than - Ít hơn</li>
				<li>greater than or equal to - Lớn hơn hoặc bằng</li>
				<li>less than or equal to - Ít hơn hoặc bằng</li>
			</ul>
		<br><li>Trên tab Settings, trong Allow, chọn một trong số các option có sẵn.</li>
		<br><li>Đặt các giá trị được yêu cầu khác, dựa trên những gì bạn chọn cho Allow và Data. Ví dụ, nếu chọn between, sau đó chọn các giá trị Minimum: và Maximum: cho các ô đó.</li>
		<br><li>Tích ô Ignore blank nếu muốn bỏ qua các khoảng cách trống.</li>
		<br><li>Nếu muốn thêm tiêu đề - Title và thông báo cho quy tắc của bạn, chọn tab Input Message, rồi gõ tiêu đề và thông báo mong muốn.</li>
		<br><li>Tích Show input message when cell is selected để hiện thông báo khi người dùng chọn hoặc trỏ chuột qua các ô lựa chọn.</li>
		<br><li>Chọn OK.</li>
		<p>Giờ, nếu người dùng nhập giá trị không hợp lệ, một pop-up sẽ hiện ra kèm thông báo “This value doesn’t match the data validation restrictions for this cell.”</p>
	    </ol>`
    },
    
]